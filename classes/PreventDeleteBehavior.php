<?php

namespace food\classes;

use Yii;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;

class PreventDeleteBehavior extends \yii\base\Behavior
{
    public $relations;

    public function init()
    {
        if (!isset($this->relations)) {
            throw new InvalidConfigException("relations attribute must be set");
        }
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'canDelete'
        ];
    }

    public function canDelete($event)
    {
        foreach ($this->relations as $relation) {
            $method = $relation['relationMethod'];
            $count = $this->owner->$method()->count();
            if ($count != 0) {
                $this->owner->addError(
                    'id',
                    "This item cannot be deleted because it's related to $count {$relation['relationName']}."
                );
                $event->isValid = false;
                $event->handled = true;
            }
        }
        return true;
    }
}
