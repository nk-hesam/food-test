<?php

use yii\db\Migration;

class m181025_185345_add_product_stock_field extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'stock', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('product', 'stock');
    }
}
