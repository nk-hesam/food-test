<?php

use yii\db\Migration;

class m181025_182145_add_order_status_field extends Migration
{
    public function up()
    {
        $this->addColumn(
            'product_order',
            'status',
            $this->smallInteger()->notNull()->defaultValue(1)->comment(
                '1:pending, 2:approved, 3:rejected'
            )
        );
    }

    public function down()
    {
        $this->dropColumn('product_order', 'status');
    }
}
