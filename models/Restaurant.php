<?php
namespace food\models;

use food\classes\PreventDeleteBehavior;

class Restaurant extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'restaurant';
    }

    public function behaviors()
    {
        return [
            [
                'class' => PreventDeleteBehavior::class,
                'relations' => [
                    [
                        'relationMethod' => 'getProducts',
                        'relationName' => 'Products'
                    ]
                ]
            ]
        ];
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'string', 'max' => 255],
            ['title', 'trim']
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['restaurantId' => 'id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['restaurantId' => 'id']);
    }

    public function extraFields()
    {
        return [
            'products',
            'orders'
        ];
    }
}
