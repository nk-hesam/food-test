<?php
namespace food\controllers;

use food\models\Restaurant;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class RestaurantController extends ActiveController
{
    public $modelClass = Restaurant::class;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete']);
        return $actions;
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete() === false) {
            return $model->getErrors();
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    protected function findModel($id)
    {
        if (($model = Restaurant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
