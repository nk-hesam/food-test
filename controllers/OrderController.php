<?php
namespace food\controllers;

use food\models\Order;
use yii\web\NotFoundHttpException;

class OrderController extends \yii\rest\Controller
{
    public function actions()
    {
        return [
            'create' => [
                'class' => 'yii\rest\CreateAction',
                'modelClass' => Order::class
            ],
        ];
    }

    public function actionApprove($id)
    {
        $order = $this->findModel($id);
        $order->approve();
        return $order;
    }

    public function actionReject($id)
    {
        $order = $this->findModel($id);
        $order->reject();
        return $order;
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}

