<?php
namespace food\models;

use food\classes\PreventDeleteBehavior;

class Product extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => PreventDeleteBehavior::class,
                'relations' => [
                    [
                        'relationMethod' => 'getOrders',
                        'relationName' => 'Orders'
                    ]
                ]
            ]
        ];
    }

    public function rules()
    {
        return [
            [['title', 'restaurantId'], 'required'],
            ['title', 'string', 'max' => 255],
            ['title', 'trim'],
            ['stock', 'integer', 'min' => 0],
            [
                'restaurantId',
                'exist',
                'targetClass' => Restaurant::class,
                'targetAttribute' => ['restaurantId' => 'id']
            ],
        ];
    }

    public function submitOrder(Order $order)
    {
        $this->stock -= $order->quantity;
        $this->save();
    }

    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurantId']);
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['productId' => 'id']);
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'stock',
            'ordersCount' => function ($model) {
                return $model->getOrders()->count();
            }
        ];
    }
}
