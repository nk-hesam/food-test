<?php

use yii\db\Migration;

class m181023_142145_create_food_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('restaurant', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull()
        ]);

        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'restaurantId' => $this->integer()->notNull(),
        ]);

        $this->createTable('product_order', [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull(),
            'restaurantId' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_rest_prod',
            'product',
            'restaurantId',
            'restaurant',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'FK_order_1',
            'product_order',
            'restaurantId',
            'restaurant',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'FK_order_2',
            'product_order',
            'productId',
            'product',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('product_order');
        $this->dropTable('product');
        $this->dropTable('restaurant');
    }
}
