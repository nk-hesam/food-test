<?php
namespace food\models;

class Order extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;

    public static function tableName()
    {
        return 'product_order';
    }

    public function rules()
    {
        return [
            [['productId', 'restaurantId', 'quantity'], 'required'],
            ['quantity', 'integer', 'min' => 1],
            [
                'productId',
                'exist',
                'targetClass' => Product::class,
                'targetAttribute' => ['productId' => 'id']
            ],
            [
                'restaurantId',
                'exist',
                'targetClass' => Restaurant::class,
                'targetAttribute' => ['restaurantId' => 'id']
            ],
        ];
    }

    public function approve()
    {
        $this->status = self::STATUS_APPROVED;
        $this->save();
    }

    public function reject()
    {
        $this->status = self::STATUS_REJECTED;
        $this->save();
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->status = self::STATUS_PENDING;
            if (!$this->isInStock()) {
                $this->addError('id', 'product is out of stock.');
                return false;
            }
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->product->submitOrder($this);
            // we also may notify restaurant about the new order here
            // better way: trigger an OrderSubmitted event and make product and restaurant listen for that
        }
        parent::afterSave($insert, $changedAttributes);
    }

    protected function isInStock()
    {
        return $this->product->stock >= $this->quantity;
    }

    public function fields()
    {
        return [
            'id',
            'product' => function ($model) {
                return $model->product->title;
            },
            'quantity',
            'statusLabel'
        ];
    }

    public function getStatusLabel()
    {
        $labels = self::getStatusLabels();
        return $labels[$this->status];
    }

    public static function getStatusLabels()
    {
        return [
            self::STATUS_PENDING => 'pending',
            self::STATUS_APPROVED => 'approved',
            self::STATUS_REJECTED => 'rejected'
        ];
    }

    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurantId']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }

}
